`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.11.2018 11:10:55
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter(
    CLK,
    RST_n,
    ENABLE,
    D_out    
    );
    input CLK, RST_n, ENABLE;
    output reg [7:0] D_out;

    always @(posedge CLK or negedge RST_n)
    begin
        if (RST_n==1'b0)
            D_out <= 0;
        else
        if (CLK == 1'b1)
            if (ENABLE == 1'b1)
                D_out <= D_out + 1;  
    end
        
 
endmodule
