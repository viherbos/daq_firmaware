`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.11.2018 11:29:19
// Design Name: 
// Module Name: counter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_tb(out);
output out;

reg clk,rst,enable;
wire [7:0] out; 

counter DUV(
            .CLK(clk),
            .RST_n(rst),
            .ENABLE(enable),
            .D_out(out));

always #10 clk = ~clk;

initial
    begin
    #0 clk = 0;
    rst = 0;
    enable = 0;
    # 50
    rst = 1;
    # 10
    enable = 1;
    # 100
    enable = 0;
    end



endmodule
